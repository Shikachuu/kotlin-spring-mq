package me.shikachuu.producer

import me.shikachuu.producer.order.Order
import me.shikachuu.producer.order.OrderProduct
import me.shikachuu.producer.order.OrderRepository
import org.junit.jupiter.api.Test
import org.mockito.Mockito
import org.springframework.amqp.rabbit.core.RabbitTemplate
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest
import org.springframework.boot.test.mock.mockito.MockBean
import org.springframework.data.repository.findByIdOrNull
import org.springframework.http.MediaType
import org.springframework.test.web.servlet.MockMvc
import org.springframework.test.web.servlet.ResultMatcher
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*
import org.springframework.test.web.servlet.result.MockMvcResultMatchers.*

@WebMvcTest
class OrderControllerTest(@Autowired val mockMvc: MockMvc) {

    @MockBean
    lateinit var orderRepository: OrderRepository;

    @MockBean
    lateinit var template: RabbitTemplate

    @Test
    fun `List orders`() {
        mockMvc.perform(get("/order").accept(MediaType.APPLICATION_JSON))
            .andExpect(
                ResultMatcher.matchAll(
                    status().isOk,
                    jsonPath("\$.content").isArray,
                    jsonPath("\$.content").isEmpty,
                    jsonPath("\$.empty").value(true),
                )
            )
    }

    @Test
    fun `Get a single Order`() {
        Mockito.`when`(orderRepository.findByIdOrNull("/asd"))
            .thenReturn(
                Order(
                    "asd",
                    "asd",
                    "asd",
                    "asd",
                    listOf(OrderProduct("asd", 1))
                )
            )
        /**
         * Negative case
         */
        mockMvc.perform(get("/order/dsa").accept(MediaType.APPLICATION_JSON))
            .andExpect(
                ResultMatcher.matchAll(
                    status().isNotFound,
                    jsonPath("\$.error").value("Not Found")
                )
            )

        /**
         * Positive case
         */
        mockMvc.perform(get("/order/asd").accept(MediaType.APPLICATION_JSON))
            .andExpect(
                ResultMatcher.matchAll(
                    status().isOk,
                    jsonPath("\$.customer_name").value("asd")
                )
            )
    }
}