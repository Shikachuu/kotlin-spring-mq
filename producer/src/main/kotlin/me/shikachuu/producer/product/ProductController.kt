package me.shikachuu.producer.product

import com.fasterxml.jackson.databind.ObjectMapper
import me.shikachuu.producer.order.OrderProduct
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.data.domain.Page
import org.springframework.data.domain.PageRequest
import org.springframework.data.repository.findByIdOrNull
import org.springframework.http.HttpStatus
import org.springframework.http.MediaType
import org.springframework.http.ResponseEntity
import org.springframework.web.bind.annotation.*
import org.springframework.web.server.ResponseStatusException

@RestController
@RequestMapping("/product", produces = [MediaType.APPLICATION_JSON_VALUE])
class ProductController {

    @Autowired
    lateinit var productRepository: ProductRepository

    @Autowired
    lateinit var encoder: ObjectMapper

    @GetMapping("")
    fun getAllProduct(
        @RequestParam(defaultValue = "0") page: Int,
        @RequestParam(defaultValue = "100") size: Int,
    ): ResponseEntity<Page<Product>> = ResponseEntity.ok(productRepository.findAll(PageRequest.of(page, size)))

    @GetMapping("/{productId}")
    fun getProduct(@PathVariable productId: String): ResponseEntity<Product> = ResponseEntity.ok(
        productRepository.findByIdOrNull(productId)
            ?: throw ResponseStatusException(
                HttpStatus.NOT_FOUND, encoder.writeValueAsString(
                    mapOf("error" to "Can't find product: $productId")
                )
            )
    )

    @PostMapping("", consumes = [MediaType.APPLICATION_JSON_VALUE])
    fun createProduct(@RequestBody product: Product): ResponseEntity<Map<String, String>> =
        ResponseEntity(mapOf("id" to productRepository.insert(product).id!!), HttpStatus.CREATED)

    @DeleteMapping("/{productId}")
    fun deleteProduct(@PathVariable productId: String): ResponseEntity<Map<String, String>> {
        productRepository.delete(
            productRepository.findByIdOrNull(productId)
                ?: throw ResponseStatusException(
                    HttpStatus.NOT_FOUND, encoder.writeValueAsString(
                        mapOf("error" to "Can't find product: $productId")
                    )
                )
        )
        return ResponseEntity(mapOf("id" to productId), HttpStatus.ACCEPTED)
    }

    @PostMapping("/stock")
    fun updateStock(@RequestBody updateStock: List<OrderProduct>): ResponseEntity<Map<String, Int>> {
        val updated = mutableMapOf<String, Int>()
        updateStock.forEach {
            val product = productRepository.findByIdOrNull(it.productId)
                ?: throw ResponseStatusException(
                    HttpStatus.NOT_FOUND, encoder.writeValueAsString(
                        mapOf("error" to "Can't find product: ${it.productId}")
                    )
                )
            product.stock += it.quantity
            productRepository.save(product)
            updated[product.id!!] = product.stock
        }
        return ResponseEntity(updated, HttpStatus.OK)
    }
}