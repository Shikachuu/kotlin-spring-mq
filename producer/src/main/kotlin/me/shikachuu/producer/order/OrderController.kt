package me.shikachuu.producer.order

import com.fasterxml.jackson.databind.ObjectMapper
import org.springframework.amqp.core.Queue
import org.springframework.amqp.rabbit.core.RabbitTemplate
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.data.domain.Page
import org.springframework.data.domain.PageRequest
import org.springframework.data.repository.findByIdOrNull
import org.springframework.http.HttpStatus
import org.springframework.http.MediaType
import org.springframework.http.ResponseEntity
import org.springframework.web.bind.annotation.*
import org.springframework.web.server.ResponseStatusException

@RestController
@RequestMapping("/order", produces = [MediaType.APPLICATION_JSON_VALUE])
class OrderController {

    @Autowired
    lateinit var orderRepository: OrderRepository

    @Autowired
    lateinit var encoder: ObjectMapper

    @Autowired
    private lateinit var template: RabbitTemplate

    @Autowired
    private lateinit var queue: Queue

    @GetMapping("")
    fun getAllOrder(
        @RequestParam(defaultValue = "0") page: Int,
        @RequestParam(defaultValue = "100") size: Int,
    ): ResponseEntity<Page<Order>> = ResponseEntity.ok(orderRepository.findAll(PageRequest.of(page, size)))

    @GetMapping("/{orderId}")
    fun getOrder(@PathVariable orderId: String): ResponseEntity<Order> = ResponseEntity.ok(
        orderRepository.findByIdOrNull(orderId)
            ?: throw ResponseStatusException(
                HttpStatus.NOT_FOUND, encoder.writeValueAsString(
                    mapOf("error" to "Can't find product: $orderId")
                )
            )
    )

    @PostMapping("", consumes = [MediaType.APPLICATION_JSON_VALUE])
    fun createOrder(@RequestBody order: Order): ResponseEntity<Map<String, String>> {
        order.content.forEach { template.convertAndSend(queue.actualName, encoder.writeValueAsString(it)) }
        return ResponseEntity(mapOf("id" to orderRepository.insert(order).id!!), HttpStatus.CREATED)
    }

}