package me.shikachuu.producer.order

import com.fasterxml.jackson.annotation.JsonAlias
import org.bson.types.ObjectId
import org.springframework.data.annotation.Id
import org.springframework.data.mongodb.core.mapping.Document

@Document(collection = "orders")
data class Order(
    @Id
    var id: String? = ObjectId().toHexString(),
    @JsonAlias("customer_name")
    val customerName: String,
    @JsonAlias("shipping_address")
    val shippingAddress: String,
    @JsonAlias("billing_address")
    val billingAddress: String,
    val content: List<OrderProduct>
)