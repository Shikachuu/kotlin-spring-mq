package me.shikachuu.consumer.product

import com.fasterxml.jackson.databind.ObjectMapper
import me.shikachuu.consumer.order.OrderProduct
import org.springframework.amqp.rabbit.annotation.RabbitListener
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.stereotype.Service

@Service
class ProductStockReceiver {

    @Autowired
    lateinit var productRepository: ProductRepository

    @Autowired
    lateinit var encoder: ObjectMapper

    @RabbitListener(queues = ["stockQueue"])
    fun receive(message: String) {
        val orderProduct = encoder.readValue(message, OrderProduct::class.java)
        productRepository.findById(orderProduct.productId).map {
            it.stock -= orderProduct.quantity
            productRepository.save(it)
        }
    }
}