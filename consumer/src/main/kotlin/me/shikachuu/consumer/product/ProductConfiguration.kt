package me.shikachuu.consumer.product

import org.springframework.amqp.core.Queue
import org.springframework.context.annotation.Bean
import org.springframework.context.annotation.Configuration

@Configuration
class ProductConfiguration {

    @Bean
    fun stockQueue() = Queue("stockQueue")
}