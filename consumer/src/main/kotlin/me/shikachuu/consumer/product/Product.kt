package me.shikachuu.consumer.product

import com.fasterxml.jackson.annotation.JsonProperty
import org.bson.types.ObjectId
import org.springframework.data.annotation.Id
import org.springframework.data.mongodb.core.mapping.Document

@Document(collection = "products")
data class Product(
    @Id
    var id: String? = ObjectId().toHexString(),
    val name: String,
    val sku: String,
    val price: Int,
    @JsonProperty(access = JsonProperty.Access.READ_ONLY)
    var stock: Int = 0
)
