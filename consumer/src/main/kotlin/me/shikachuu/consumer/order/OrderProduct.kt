package me.shikachuu.consumer.order

import com.fasterxml.jackson.annotation.JsonAlias

data class OrderProduct(
    @JsonAlias("product_id")
    val productId: String,
    val quantity: Int
)